﻿<?php defined('INDIRECT_ACCESS') or define('INDIRECT_ACCESS', true);
require_once "helper.php";
require_once "query.php";

$inv_id = $checkResult();

// проверка корректности подписи
// check signature
if ($inv_id == -1)
    die("bad sign\n");

// признак успешно проведенной операции
// success
$payOrder($inv_id);
die("OK$inv_id\n");