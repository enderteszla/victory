<?php defined('INDIRECT_ACCESS') or define('INDIRECT_ACCESS', true);
require_once "helper.php";
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Подари победу! Будь частью Великой истории!</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="js/jquery-1.10.2.min.js"></script>
    <link href="plugins/formstyler/jquery.formstyler.css" rel="stylesheet"/>
    <script src="plugins/formstyler/jquery.formstyler.js"></script>
    <script src="plugins/animate/assets/javascripts/cs.script.js"></script>
    <link href="plugins/animate/assets/stylesheets/cs.animate.css" rel="stylesheet"/>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <link href="css/styles.css" rel="stylesheet"/>
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css" media="all">
    <script type="text/javascript">
        (function ($) {
            $(function () {
                $('input, select').not(':disabled').styler({
                    selectSearch: true
                });
            });
        })(jQuery);


        $(function () {

            $('.dblb').on('click', function (e) {
                $('html,body').stop().animate({scrollTop: $('#hoverblock').offset().top}, 700);
                e.preventDefault();
            });

        });


        $(function () {

            $('.dffdfd').on('click', function (e) {
                $('html,body').stop().animate({scrollTop: $('#hoverblock').offset().top}, 700);
                e.preventDefault();
            });

        });


        $(document).ready(function () {

            //main nav
            $(window).on('scroll load', function () {
                updateMainNav();
            });

            function updateMainNav() {
                if ($(window).scrollTop() >= 50) {
                    $('body').addClass('minimize-menu');
                } else {
                    $('body').removeClass('minimize-menu');
                }
            }

            $('.user-nav > a').on('click', function () {
                $('body').toggleClass('show-user-nav');
            });

            $(document).on('click', function (event) {
                $('body').removeClass('show-user-nav');
            });

            $('.collapse-main-nav').on('click', function () {
                if ($('body').toggleClass('show-main-nav').hasClass('show-main-nav')) window.scrollTo(0, 0);
                return false;
            });

        });
    </script>
</head>
<body>
<header>
    <div class="container">

        <div class="fll txtz">
            Подари победу!
                <span
                    class="hidden-xs hidden-md hidden-sm">Будь частью Великой истории!</span>
        </div>

        <div class="mail-soc">
            <a class="mail-a" href="mailto:info@podaripobedy.ru"> <i class="fa fa-envelope-o"></i>
                info@podaripobedy.ru
            </a>
                <span class="phone-a"> <i class="fa fa-phone"></i>
                    8 (495) 134-31-08 <strong>|</strong>
                    <i class="fa fa-phone"></i>
                    8 (800) 505-39-61
                </span>

            <!--
                                <div class="clearfix">
            <ul id="toper" class="list-inline">

                <li class="soc-size-head wobble-horizontal">
                    <a target="_blank" href="http://vk.com/rospt" class="btooltip swing" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Вконтакте">
                        <i class="soc-size-header fa fa-vk red size soc1"></i>
                    </a>
                </li>
                <li class="soc-size-head wobble-horizontal">
                    <a target="_blank" href="https://www.facebook.com/groups/741747129244478/" class="btooltip swing ne-he" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Фейсбук">
                        <i class="fa fa-facebook red size soc2  soc-size-header"></i>
                    </a>
                </li>
                <li class="soc-size-head wobble-horizontal">
                    <a target="_blank" href="https://twitter.com/Ros_Patriot" class="btooltip swing" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Твиттер">
                        <i class="fa fa-twitter red size soc3 soc-size-header"></i>
                    </a>
                </li>
                <li class="soc-size-head wobble-horizontal">
                    <a target="_blank" href="http://instagram.com/ros_patriot" class="btooltip swing" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Инстаграм">
                        <i class="fa fa-instagram red size soc4 soc-size-header"></i>
                    </a>
                </li>
            </ul>
        </div>
        -->
        </div>
    </div>
</header>

<div id="hoverblock" class="ims4">
    <div class="container">
        <h2 class="not-animated" data-animate="fadeInUp" data-delay="200">Оставить заявку на бесплатную футболку</h2>
        <form action="/order2.php" method="post" enctype="multipart/form-data" class=" mt-form-fix">
            <div class="col-md-12 col-sm-12 col-xs-12 subform">

                <?php $value = get_value("first_name"); ?>
                <label class="srtys">
                    Введите имя / название организация (дет. дом и т.п.)
                    <span>*</span>
                </label>
                <input type="text" name="first_name"
                       class="formname <?php if (has_error("first_name")) echo "error"; ?>"
                       size="40" placeholder="Например: Детский дом № 1" value="<?php echo $value; ?>">

                <?php $value = get_value("last_name"); ?>
                <label>Введите фамилию</label>
                <input type="text" name="last_name" class="formname <?php if (has_error("last_name")) echo "error"; ?>"
                       size="40" placeholder="Фамилия" value="<?php echo $value; ?>">

                <?php $value = get_value("phone"); ?>
                <label>
                    Введите телефон
                    <span>*</span>
                </label>
                <input type="tel" name="phone" class="formname <?php if (has_error("phone")) echo "error"; ?>"
                       size="40" placeholder="Телефон" value="<?php echo $value; ?>">

                <?php $value = get_value("email"); ?>
                <label>
                    Введите электронную почту
                    <span>*</span>
                </label>
                <input type="email" name="email" class="formname <?php if (has_error("email")) echo "error"; ?>"
                       size="40" placeholder="E-mail" value="<?php echo $value; ?>">

                <?php $value = get_value("address"); ?>
                <label>Адрес <span>*</span></label>
                <textarea rows="10" name="address" class="formtext <?php if (has_error("address")) echo "error"; ?>"
                          cols="30"
                          placeholder="Адрес"><?php echo $value; ?></textarea>

                <?php $value = get_value("info"); ?>
                <label>Уточнения по заказу</label>
                <textarea rows="10" name="info" class="formtext <?php if (has_error("info")) echo "error"; ?>" cols="30"
                          placeholder="Сообщение"><?php echo $value; ?></textarea>

                <label class="ndt">
                    Прикрепитие ваше изображение для печати
                </label>
                <input name="attachment" type="file"
                       size="28" <?php if (has_error("attachment")) echo "class=\"error\""; ?>>
                <label class="ndt"><span style="color: red">*</span> Срок изготовления футболки от двух дней</label>
                <label class="ndt">
                    <a class="tbl hidden-xs" data-toggle="modal" href="#" data-target="#smallModal"> <br>
                        <span style="color: red">*</span> Требования предъявляемые к фотографии
                    </a>

                </label>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 subform">
                <button class="btn-sub" type="submit">Оставить заявку</button>
            </div>
        </form>
    </div>
</div>
<footer>
    <div class="container">
        <div id="footer-content2">
            <div class="row footer-content-bottom">
                <div id="widget-linklist4" class="col-sm-15">
                    <div class="widget-wrapper">
                        <ul class="list-unstyled">
                            <li>
                                <div class="whites">
                                        <span class="phone-a">
                                            <i class="fa fa-phone"></i>
                                            8 (495) 134-31-08
                                        </span>
                                    <span class="sp-ff"></span>
                                        <span class="phone-a">
                                            <i class="fa fa-phone"></i>
                                            8 (800) 505-39-61
                                            <strong class="hidden-xs hidden-sm hidden-md">Для регионов</strong>
                                        </span>
                                </div>
                            </li>
                        </ul>

                        <a target="_blank" class="insta" href="https://www.instagram.com/podari_pobedy/">
                                <span
                                    class="hidden-xs hidden-sm hidden-md">Наш Instargam:</span>
                            <i
                                class="fa fa-instagram"></i>
                        </a>

                        <div class="copyright">
                            © 2014 RosPatriot.
                            <a class="mail-a" href="mailto:info@podaripobedy.ru">info@podaripobedy.ru</a>
                        </div>
                    </div>
                    <div id="widget-payment" class="col-sm-5 hidden-xs"></div>
                </div>
            </div>
        </div>
    </div>
</footer>


<div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Требования предъявляемые к фотографии</h4>
            </div>
            <div class="modal-body">


                <table>
                    <tbody>
                    <tr>
                        <td class="cap ptop">Формат изображения</td>
                        <td class="ptop">.JPG, .PNG, .GIF, .TIFF (Вы можете предоставить изображение и в другом формате,
                            но тогда возможны потери цвета при конвертации)
                        </td>
                    </tr>
                    <tr>
                        <td class="cap">Разрешение файла</td>

                        <td>не менее 150 dpi (то есть файл который вы хотите распечатать форматом A4 должен быть не
                            менее 1700px по большей стороне, в противном случае возможна потеря качества при печати)
                        </td>
                    </tr>
                    <tr>
                        <td class="cap">Максимальная область печати</td>
                        <td>40см*50см</td>
                    </tr>

                    </tbody>
                </table>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть окно</button>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Таблица размеров</h4>
            </div>
            <div class="modal-body">

                <h3 class="fsc">Мужская футболка</h3>
                <table>
                    <tbody>
                    <tr>
                        <td class="cap">Европейские размеры</td>
                        <td class="esize">S</td>
                        <td class="esize">M</td>
                        <td class="esize">L</td>
                        <td class="esize">XL</td>
                        <td class="esize">XXL</td>
                        <td class="esize">XXXL</td>
                    </tr>
                    <tr>
                        <td class="cap">Российские размеры</td>
                        <td>44-46</td>
                        <td>46-48</td>
                        <td>48-50</td>
                        <td>50-52</td>
                        <td>52-54</td>
                        <td>54-56</td>
                    </tr>
                    <tr>
                        <td class="cap">Высота (см.)</td>
                        <td>68</td>
                        <td>71</td>
                        <td>74</td>
                        <td>77</td>
                        <td>78</td>
                        <td>80</td>
                    </tr>
                    <tr>
                        <td class="cap">Ширина (см.)</td>
                        <td>50</td>
                        <td>51</td>
                        <td>55</td>
                        <td>58</td>
                        <td>61</td>
                        <td>63</td>
                    </tr>
                    </tbody>
                </table>

                <h3>Женская футболка</h3>

                <table>
                    <tbody>
                    <tr>
                        <td class="cap">Европейские размеры</td>
                        <td class="esize">S</td>
                        <td class="esize">M</td>
                        <td class="esize">L</td>
                        <td class="esize">XL</td>
                        <td class="esize">XXL</td>
                    </tr>
                    <tr>
                        <td class="cap">Российские размеры</td>
                        <td>40-42</td>
                        <td>42-44</td>
                        <td>44-46</td>
                        <td>48</td>
                        <td>50</td>
                    </tr>
                    <tr>
                        <td class="cap">Высота (см.)</td>
                        <td>60</td>
                        <td>62</td>
                        <td>64</td>
                        <td>66</td>
                        <td>68</td>
                    </tr>
                    <tr>
                        <td class="cap">Ширина (см.)</td>
                        <td>42</td>
                        <td>44</td>
                        <td>46</td>
                        <td>47</td>
                        <td>49</td>
                    </tr>
                    </tbody>
                </table>

                <h3>Детская футболка</h3>
                <table>
                    <tbody>
                    <tr>
                        <td class="cap">Возраст</td>
                        <td class="esize">2-3 года</td>
                        <td class="esize">4-5 лет</td>
                        <td class="esize">6-7 лет</td>
                        <td class="esize">8-9 лет</td>
                        <td class="esize">10-11 лет</td>
                        <td class="esize">12-14 лет</td>
                    </tr>
                    <tr>
                        <td class="cap">Европейские размеры</td>
                        <td class="esize">5XS</td>
                        <td class="esize">4XS</td>
                        <td class="esize">3XS</td>
                        <td class="esize">2XS</td>
                        <td class="esize">XS</td>
                        <td class="esize">S</td>
                    </tr>
                    <tr>
                        <td class="cap">Российские размеры</td>
                        <td>24-26</td>
                        <td>28-30</td>
                        <td>32-36</td>
                        <td>36-38</td>
                        <td>38-40</td>
                        <td>40-42</td>
                    </tr>
                    <tr>
                        <td class="cap">Высота (см.)</td>
                        <td>40</td>
                        <td>44</td>
                        <td>49</td>
                        <td>56</td>
                        <td>58</td>
                        <td>59</td>
                    </tr>
                    <tr>
                        <td class="cap">Ширина (см.)</td>
                        <td>31</td>
                        <td>34</td>
                        <td>37</td>
                        <td>40</td>
                        <td>42</td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td class="cap">Рост (см.)</td>
                        <td>110</td>
                        <td>120</td>
                        <td>130</td>
                        <td>140</td>
                        <td>150</td>
                        <td>160</td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть окно</button>

            </div>
        </div>
    </div>
</div>
<?php $message = get_value('message'); ?>
<?php if ($message): ?>
    <div class="info"><?php echo $message; ?></div>
<?php endif; ?>
<script>
    $(document).ready(function () {
        $(".info").fadeOut(6000);
    });
</script>

<style>
    header {
        background: rgba(57, 67, 222, 0.81) !important;
    }

    div#hoverblock {
        padding-top: 55px;
    }
</style>
</body>
</html>