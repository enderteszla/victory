<?php defined('INDIRECT_ACCESS') or define('INDIRECT_ACCESS', true);
require_once "helper.php";
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Подари победу! Будь частью Великой истории!</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="js/jquery-1.10.2.min.js"></script>
    <link href="plugins/formstyler/jquery.formstyler.css" rel="stylesheet"/>
    <script src="plugins/formstyler/jquery.formstyler.js"></script>
    <script src="plugins/animate/assets/javascripts/cs.script.js"></script>
    <link href="plugins/animate/assets/stylesheets/cs.animate.css" rel="stylesheet"/>
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <link href="css/styles.css" rel="stylesheet"/>
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css" media="all">
    <script type="text/javascript">
        (function ($) {
            $(function () {
                $('input, select').not(':disabled').styler({
                    selectSearch: true
                });
            });
        })(jQuery);


        $(function () {

            $('.dblb').on('click', function (e) {
                $('html,body').stop().animate({scrollTop: $('#hoverblock').offset().top}, 700);
                e.preventDefault();
            });

        });
        
        
           
        
        $(function () {

            $('.dffdfd').on('click', function (e) {
                $('html,body').stop().animate({scrollTop: $('#hoverblock').offset().top}, 700);
                e.preventDefault();
            });

        });




        $(document).ready(function () {

            //main nav
            $(window).on('scroll load', function () {
                updateMainNav();
            });

            function updateMainNav() {
                if ($(window).scrollTop() >= 50) {
                    $('body').addClass('minimize-menu');
                } else {
                    $('body').removeClass('minimize-menu');
                }
            }

            $('.user-nav > a').on('click', function () {
                $('body').toggleClass('show-user-nav');
            });

            $(document).on('click', function (event) {
                $('body').removeClass('show-user-nav');
            });

            $('.collapse-main-nav').on('click', function () {
                if ($('body').toggleClass('show-main-nav').hasClass('show-main-nav')) window.scrollTo(0, 0);
                return false;
            });

        });
    </script>
</head>
<body>
<header>
    <div class="container">

        <div class="fll txtz">
            Подари победу!
                <span
                    class="hidden-xs hidden-md hidden-sm">Будь частью Великой истории!</span>
        </div>

        <div class="mail-soc">
            <a class="mail-a" href="mailto:info@podaripobedy.ru"> <i class="fa fa-envelope-o"></i>
                info@podaripobedy.ru
            </a>
                <span class="phone-a"> <i class="fa fa-phone"></i>
                    8 (495) 134-31-08 <strong>|</strong>
                    <i class="fa fa-phone"></i>
                    8 (800) 505-39-61
                </span>

            <!--
                                <div class="clearfix">
            <ul id="toper" class="list-inline">

                <li class="soc-size-head wobble-horizontal">
                    <a target="_blank" href="http://vk.com/rospt" class="btooltip swing" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Вконтакте">
                        <i class="soc-size-header fa fa-vk red size soc1"></i>
                    </a>
                </li>
                <li class="soc-size-head wobble-horizontal">
                    <a target="_blank" href="https://www.facebook.com/groups/741747129244478/" class="btooltip swing ne-he" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Фейсбук">
                        <i class="fa fa-facebook red size soc2  soc-size-header"></i>
                    </a>
                </li>
                <li class="soc-size-head wobble-horizontal">
                    <a target="_blank" href="https://twitter.com/Ros_Patriot" class="btooltip swing" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Твиттер">
                        <i class="fa fa-twitter red size soc3 soc-size-header"></i>
                    </a>
                </li>
                <li class="soc-size-head wobble-horizontal">
                    <a target="_blank" href="http://instagram.com/ros_patriot" class="btooltip swing" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Инстаграм">
                        <i class="fa fa-instagram red size soc4 soc-size-header"></i>
                    </a>
                </li>
            </ul>
        </div>
        -->
        </div>
    </div>
</header>
<div class="ims">

    <div class="container"></div>
</div>
<div class="ims3">
    <div class="container">
        <h2 class="not-animated" data-animate="fadeInUp" data-delay="200">ПОДВИГ ПРЕДКОВ БУДЕМ ПОМНИТЬ ВСЕГДА</h2>

        <div class="col-md-12 col-sm-12 col-xs-12 not-animated" data-animate="fadeIn" data-delay="200">
            <p class="mx-pp">
                Хотите сделать незабываемый подарок к 9 мая? Мы предлагаем вам такую возможность. Закажите футболку
                белого цвета с фотографией вашего родственника сделавшего победу в Великой Отечественной войне реальной!
                Для этого вам нужно всего лишь заполнить форму ниже и мы с удовольствием подготовим для Вас этот ценный
                подарок!
                <br><br><span class="red-str">*</span> <span class="bluer">Реализация продаж футболок с вашими фотографиями начнётся позже. Следите за новостями на нашем сайте.</span>
            </p>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            <p class="zab1 tlbs">Мужская</p>
            <img src="img/m1.png" class="not-animated dblb" data-animate="swing" data-delay="400">
          <!--  <p class="zab2">1000 руб.</p> -->
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            <p class="zab1 tlbs">Женская</p>
            <img src="img/m2.png" class="not-animated dblb" data-animate="swing" data-delay="1400">
           <!-- <p class="zab2">1000 руб.</p> -->
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <p class="zab1 tlbs detsc-p">Детская</p>
            <img src="img/m3sw2.png" class="not-animated dblb detsc" data-animate="swing" data-delay="1000">
           <!-- <p class="zab2">1000 руб.</p> -->
        </div>

    </div>
</div>
<div class="imss">
    <div class="container">

        <div class="col-md-12 col-sm-12 col-xs-12 not-animated" data-animate="fadeIn" data-delay="200">
            <h2 class="fadeInUp animated colww" data-animate="fadeInUp" data-delay="200">
                МЫ ЗАЩИТИЛИ ОТЧИЗНУ! МЫ ВЗЯЛИ
                БЕРЛИН!
                <br>МЫ ПОБЕДИЛИ!</h2>
            <div class="zv2"></div>
            <p class="mx-pp">1941-1945</p>
            
            <p class="ppsaa">Каждый желающий может отправить нам на почту снимки своих родственников - участников Великой Отечественной войны вместе с историями военных лет связанных с ними. В инстаграме podari_pobedy мы будем их публиковать!</p>
            
            <a target="_blank" class="insta paperts" href="https://www.instagram.com/podari_pobedy/">
                                <span class="hidden-xs hidden-sm hidden-md">Instargam:</span>
                            <i class="fa fa-instagram"></i>
                        </a>

        </div>

    </div>
</div>
<div class="imsb">
    <div class="full-bg">
        <div class="container reww">
            <!--
            <div class="hidden-md hidden-sm hidden-xs stql fll not-animated blink-1" data-animate="bounceIn"
                 data-delay="200">Великий праздник!</div>
        <div class="hidden-md hidden-sm hidden-xs stql2 flr not-animated blink-2" data-animate="bounceIn"
                 data-delay="1200">Подарить победу!</div>
        -->
            <div class="hidden-xs stql3 flr not-animated blink-3" data-animate="bounceIn"
                 data-delay="800">
                Уникальная возможность
                <br>
                подарить память
                <br>о Великом дне!
            </div>

        </div>

    </div>

    <div class="srts">
        Другие варианты футболок можно посмотреть на сайте партнера акции
        <a
            href="http://www.rospatriot.ru"
            target="_blank">www.rospatriot.ru</a>
    </div>

    <div class="">
    <div class="imsb gray">
        <div class="container">
            <h2 class="not-animated" data-animate="fadeInUp" data-delay="200">Партнеры акции</h2>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h4>Партнеры</h4>

                <ul class="calf">
                    <li>
                        <a target="_blank" href="http://rospatriot.ru">
                            <img class="img-responsive" src="img/logo.png" alt=""/>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="http://needeachother.ru">
                            <img class="img-responsive" src="img/logos.png" alt=""/>
                        </a>
                    </li>
                  
                      <li>
                        <a target="_blank" href="http://skypasser.ru">
                            <img class="img-responsive" src="img/sk.png" alt=""/>
                        </a>
                    </li>

                </ul>

            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h4>Информационные партнеры</h4>

                <ul class="calf">
                    <li>
                        <a target="_blank" href="http://rutoday.com">
                            <img class="img-responsive" src="img/logo-ru.png" alt=""/>
                        </a>
                    </li>

                </ul>

            </div>
           
                    <div class="col-md-12 col-sm-12 col-xs-12">
            <h4>При поддержке</h4>

          <ul class="calf calfsw">
                <li>
                        <a target="_blank" href="http://soyuzveteranov.ru/?q=content/kontakty-0">
                            <img class="img-responsive" src="img/bp1.png" alt=""/>
                        </a>
                    </li>
                    
                    <li>
                        <a target="_blank" href="http://www.mgsv.org/page/1.html">
                            <img class="img-responsive" src="img/bp2.png" alt=""/>
                        </a>
                    </li>
                    
                    <li>
                        <a target="_blank" href="http://materirossii.ru/contacts/">
                            <img class="img-responsive" src="img/bp3.png" alt=""/>
                        </a>
                    </li>

            </ul>

        </div>
      
        </div>
    </div>

    <div class="fllg">

        <div class="container">
            <h2 class="not-animated podars" data-animate="fadeInUp" data-delay="200">Подари победу</h2>
            <!--<div class="qw not-animated" data-animate="bounceIn" data-delay="200"></div>
        -->
            <p class="mx-pp black">
                Вместе с партнерами акции мы дарим возможность детским домам, интернатам, организациям по работе с людьми с
                ограниченными возможностями, а также малоимущим жителям получить футболку с принтами
                <br>
                "9 мая"
                бесплатно!
                <br>
                <br>
                 Для этого связывайтесь с нашими менеджерами по телефону указанному на сайте и адресу электронной почты с
                пометкой «Футболка даром – подари победу» или заполните специальную форму:
                       <a href=""><div class="btn-sub dffdfd">Заполнить форму</div></a>
            </p>

            <div class="mail-soc mms2">
                <a class="mail-a" href="mailto:info@podaripobedy.ru">
                    <i class="fa fa-envelope-o"></i>
                    info@podaripobedy.ru
                </a>
    <span class="phone-a">
        <i class="fa fa-phone"></i>
        8 (495) 134-31-08
    </span>
    <span class="phone-a sp-ff">
        <i class="fa fa-phone"></i>
        8 (800) 505-39-61
    </span>
            </div>

        </div>

    </div>
</div>
<div id="hoverblock" class="ims4">
    <div class="container">
        <h2 class="not-animated" data-animate="fadeInUp" data-delay="200">Обратная связь</h2>
        <form action="/order2.php" method="post" enctype="multipart/form-data" class=" mt-form-fix">
            <div class="col-md-12 col-sm-12 col-xs-12 subform">

                <?php $value = get_value("first_name"); ?>
                <label class="srtys">
                   Введите имя / название организация (дет. дом и т.п.)
                    <span>*</span>
                </label>
                <input type="text" name="first_name" class="formname <?php if (has_error("first_name")) echo "error"; ?>"
                       size="40" placeholder="Например: Детский дом № 1" value="<?php echo $value;?>">

                <?php $value = get_value("last_name"); ?>
                <label>Введите фамилию</label>
                <input type="text" name="last_name" class="formname <?php if (has_error("last_name")) echo "error"; ?>"
                       size="40" placeholder="Фамилия" value="<?php echo $value;?>">

                <?php $value = get_value("phone"); ?>
                <label>
                    Введите телефон
                    <span>*</span>
                </label>
                <input type="tel" name="phone" class="formname <?php if (has_error("phone")) echo "error"; ?>"
                       size="40" placeholder="Телефон" value="<?php echo $value;?>">

                <?php $value = get_value("email"); ?>
                <label>
                    Введите электронную почту
                    <span>*</span>
                </label>
                <input type="email" name="email" class="formname <?php if (has_error("email")) echo "error"; ?>"
                       size="40" placeholder="E-mail" value="<?php echo $value;?>">
                       
                <?php $value = get_value("address"); ?>
                <label>Адрес  <span>*</span></label>
                <textarea rows="10" name="address" class="formtext <?php if (has_error("address")) echo "error"; ?>" cols="30"
                          placeholder="Адрес"><?php echo $value;?></textarea>

                <?php $value = get_value("info"); ?>
                <label>Уточнения по заказу</label>
                <textarea rows="10" name="info" class="formtext <?php if (has_error("info")) echo "error"; ?>" cols="30"
                          placeholder="Сообщение"><?php echo $value;?></textarea>

                <label class="ndt">
                 Прикрепитие ваше изображение для печати
                </label>
                <input name="attachment" type="file"
                       size="28" <?php if (has_error("attachment")) echo "class=\"error\""; ?>>
                <label class="ndt"><span style="color: red">*</span> Срок изготовления футболки от двух дней</label>
                  <label class="ndt">
               <a class="tbl hidden-xs" data-toggle="modal" href="#" data-target="#smallModal"> <br>
               <span style="color: red">*</span> Требования предъявляемые к фотографии
                </a>
               
                </label>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 subform">
                <button class="btn-sub" type="submit">Оставить заявку</button>
            </div>
        </form>
    </div>
</div>
<footer>
    <div class="container">
        <div id="footer-content2">
            <div class="row footer-content-bottom">
                <div id="widget-linklist4" class="col-sm-15">
                    <div class="widget-wrapper">
                        <ul class="list-unstyled">
                            <li>
                                <div class="whites">
                                        <span class="phone-a">
                                            <i class="fa fa-phone"></i>
                                            8 (495) 134-31-08
                                        </span>
                                    <span class="sp-ff"></span>
                                        <span class="phone-a">
                                            <i class="fa fa-phone"></i>
                                            8 (800) 505-39-61
                                            <strong class="hidden-xs hidden-sm hidden-md">Для регионов</strong>
                                        </span>
                                </div>
                            </li>
                        </ul>

                        <a target="_blank" class="insta" href="https://www.instagram.com/podari_pobedy/">
                                <span
                                    class="hidden-xs hidden-sm hidden-md">Наш Instargam:</span>
                            <i
                                class="fa fa-instagram"></i>
                        </a>

                        <div class="copyright">
                            © 2014 RosPatriot.
                            <a class="mail-a" href="mailto:info@podaripobedy.ru">info@podaripobedy.ru</a>
                        </div>
                    </div>
                    <div id="widget-payment" class="col-sm-5 hidden-xs"></div>
                </div>
            </div>
        </div>
    </div>
</footer>




<div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Требования предъявляемые к фотографии</h4>
            </div>
            <div class="modal-body">

                
<table>
				<tbody><tr>
					<td class="cap ptop">Формат изображения</td>
					<td class="ptop">.JPG, .PNG, .GIF, .TIFF (Вы можете предоставить изображение и в другом формате, но тогда возможны потери цвета при конвертации)</td>
				</tr>
				<tr>
					<td class="cap">Разрешение файла</td>

					<td>не менее 150 dpi (то есть файл который вы хотите распечатать форматом A4 должен быть не менее 1700px по большей стороне, в противном случае возможна потеря качества при печати) </td>
				</tr>
				<tr>
					<td class="cap">Максимальная область печати</td>
					<td>40см*50см</td>
				</tr>
				
			</tbody></table>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть окно</button>

            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Таблица размеров</h4>
            </div>
            <div class="modal-body">

                <h3 class="fsc">Мужская футболка</h3>
                <table>
                    <tbody>
                    <tr>
                        <td class="cap">Европейские размеры</td>
                        <td class="esize">S</td>
                        <td class="esize">M</td>
                        <td class="esize">L</td>
                        <td class="esize">XL</td>
                        <td class="esize">XXL</td>
                        <td class="esize">XXXL</td>
                    </tr>
                    <tr>
                        <td class="cap">Российские размеры</td>
                        <td>44-46</td>
                        <td>46-48</td>
                        <td>48-50</td>
                        <td>50-52</td>
                        <td>52-54</td>
                        <td>54-56</td>
                    </tr>
                    <tr>
                        <td class="cap">Высота (см.)</td>
                        <td>68</td>
                        <td>71</td>
                        <td>74</td>
                        <td>77</td>
                        <td>78</td>
                        <td>80</td>
                    </tr>
                    <tr>
                        <td class="cap">Ширина (см.)</td>
                        <td>50</td>
                        <td>51</td>
                        <td>55</td>
                        <td>58</td>
                        <td>61</td>
                        <td>63</td>
                    </tr>
                    </tbody>
                </table>

                <h3>Женская футболка</h3>

                <table>
                    <tbody>
                    <tr>
                        <td class="cap">Европейские размеры</td>
                        <td class="esize">S</td>
                        <td class="esize">M</td>
                        <td class="esize">L</td>
                        <td class="esize">XL</td>
                        <td class="esize">XXL</td>
                    </tr>
                    <tr>
                        <td class="cap">Российские размеры</td>
                        <td>40-42</td>
                        <td>42-44</td>
                        <td>44-46</td>
                        <td>48</td>
                        <td>50</td>
                    </tr>
                    <tr>
                        <td class="cap">Высота (см.)</td>
                        <td>60</td>
                        <td>62</td>
                        <td>64</td>
                        <td>66</td>
                        <td>68</td>
                    </tr>
                    <tr>
                        <td class="cap">Ширина (см.)</td>
                        <td>42</td>
                        <td>44</td>
                        <td>46</td>
                        <td>47</td>
                        <td>49</td>
                    </tr>
                    </tbody>
                </table>

                <h3>Детская футболка</h3>
                <table>
                    <tbody>
                    <tr>
                        <td class="cap">Возраст</td>
                        <td class="esize">2-3 года</td>
                        <td class="esize">4-5 лет</td>
                        <td class="esize">6-7 лет</td>
                        <td class="esize">8-9 лет</td>
                        <td class="esize">10-11 лет</td>
                        <td class="esize">12-14 лет</td>
                    </tr>
                    <tr>
                        <td class="cap">Европейские размеры</td>
                        <td class="esize">5XS</td>
                        <td class="esize">4XS</td>
                        <td class="esize">3XS</td>
                        <td class="esize">2XS</td>
                        <td class="esize">XS</td>
                        <td class="esize">S</td>
                    </tr>
                    <tr>
                        <td class="cap">Российские размеры</td>
                        <td>24-26</td>
                        <td>28-30</td>
                        <td>32-36</td>
                        <td>36-38</td>
                        <td>38-40</td>
                        <td>40-42</td>
                    </tr>
                    <tr>
                        <td class="cap">Высота (см.)</td>
                        <td>40</td>
                        <td>44</td>
                        <td>49</td>
                        <td>56</td>
                        <td>58</td>
                        <td>59</td>
                    </tr>
                    <tr>
                        <td class="cap">Ширина (см.)</td>
                        <td>31</td>
                        <td>34</td>
                        <td>37</td>
                        <td>40</td>
                        <td>42</td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td class="cap">Рост (см.)</td>
                        <td>110</td>
                        <td>120</td>
                        <td>130</td>
                        <td>140</td>
                        <td>150</td>
                        <td>160</td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть окно</button>

            </div>
        </div>
    </div>
</div>
<?php $message = get_value('message');?>
<?php if($message): ?>
    <div class="info"><?php echo $message;?></div>
<?php endif; ?>
<script>
    $(document).ready(function () {
        $(".info").fadeOut(6000);
    });
</script>
</body>
</html>