<?php defined('INDIRECT_ACCESS') or define('INDIRECT_ACCESS', true);
require_once "helper.php";

$items = $_POST['items'];
$errors = [];
foreach ($items as $i => $item){
    $errors["items_{$i}_size"] = in_array($item['size'], array_keys($sizes));
    $errors["items_{$i}_lettering_type"] = in_array($item['lettering_type'], ['no', 'black', 'golden', 'blue']);
    $lo = !$item['lettering_type'] || $item['lettering_type'] == "no" || $item['lettering_option'];
    $errors["items_{$i}_lettering_option"] = $lo;
    $l = !$item['lettering_type'] || $item['lettering_type'] == "no" || !$item['lettering_option'] || $item['lettering_option'] != "Свой вариант" || $item['lettering'];
    $errors["items_{$i}_lettering"] = $l;
    $errors["items_{$i}_type"] = in_array($item['type'], ['male', 'female', 'child']);
}

$first_name = $_POST['first_name']; // required
$errors["first_name"] = !!$first_name;

$phone = $_POST['phone']; // required && phone
$errors["phone"] = !!$phone;

$email = $_POST['email']; // required && email
$errors["email"] = !!$email && preg_match("/[0-9a-z_]+@[0-9a-z_^\.]+\.[a-z]{2,3}/i", $email);

$delivery = $_POST['delivery']; // required && courier|pickup|post
$errors["delivery"] = in_array($delivery, ['courier', 'pickup', 'post']);

$address = $_POST['address'];
$errors["address"] = !!$address || $delivery == 'pickup';

$payment = $_POST['payment']; // required && cash|credit
$errors["payment"] = in_array($payment, ['cash', 'credit']);

die(json_encode($errors));