<?php if(!defined('INDIRECT_ACCESS') || !INDIRECT_ACCESS) die('No direct access allowed.');
require_once "private/database.php";

$link = new mysqli($host, $user, $pass, $db);
if ($link->connect_error)
    die('Ошибка подключения (' . $link->connect_errno . ') '
        . $link->connect_error);

$createOrder = function($data) use($link){
    $into = implode(", ", array_map(function($k){return "`$k`";},array_keys($data)));
    $values = implode(", ", array_map(function($v){return "'$v'";},array_values($data)));
    $link->query("INSERT INTO `orders`($into) VALUES($values);");
    return $link->insert_id;
};

$payOrder = function($id) use ($link){
    $link->query("UPDATE `orders` SET `paid` = TRUE WHERE `id` = $id");
};

$migrate = function() use($link){
    $link->query("CREATE TABLE IF NOT EXISTS `orders`(
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`first_name` VARCHAR(255),
`last_name` VARCHAR(255),
`email` VARCHAR(255),
`phone` VARCHAR(255),
`info` VARCHAR(255),
`delivery` VARCHAR(255),
`address` TEXT,
`sum` INT,
`payment` VARCHAR(255),
`paid` BOOLEAN DEFAULT FALSE,
`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);");
};