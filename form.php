<form action="/order.php" method="post" enctype="multipart/form-data">
    <div class="col-md-6 col-sm-6 col-xs-12">

        <?php $value = get_value("items/0/type"); ?>
        <div class="clearfix">
            <label class="spo1">
                Выберите тип
                <span>*</span>
            </label>
            <div id="items_0_type_error" class="clearfix items_error <?php if (has_error("items/0/type")) echo "error"; ?>">
                <div class="col-md-2 mtdx">
                    <input type="radio" id="items_0_type_male" name="items[0][type]" value="male" class="type"
                        <?php if ($value == "male") echo "checked"; ?>>
                    <span>Мужская</span>
                </div>
                <div class="col-md-3 mtdx">
                    <input type="radio" id="items_0_type_female" name="items[0][type]" value="female" class="type"
                        <?php if ($value == "female") echo "checked"; ?>>
                    <span>Женская</span>
                </div>
                <div class="col-md-2 mtdx mx-xv">
                    <input type="radio" id="items_0_type_child" name="items[0][type]" value="child" class="type"
                        <?php if ($value == "child") echo "checked"; ?>>
                    <span>Детская</span>
                </div>
            </div>
        </div>

        <?php $value = get_value("items/0/size"); ?>
        <div class="clearfix mtsize">
            <label class="swrt">
                Укажите желаемый размер футболки
                <span>*</span>
                <a class="tbl hidden-xs" data-toggle="modal" href="#" data-target="#largeModal">
                    (Таблица
                    размеров)
                </a>
            </label>
            <select id="items_0_size" name="items[0][size]" data-default="" class="items_error <?php if (has_error("items/0/size")) echo "error"; ?>">
                <option disabled <?php if (!$value) echo "selected"; ?> value class="male female child">Выберите
                </option>
                <?php foreach ($sizes as $size => $types): ?>
                    <option value="<?php echo $size; ?>" <?php if ($value == $size) echo "selected"; ?>
                            class="<?php echo implode(" ", $types); ?>">
                        <?php echo $size; ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <?php $value = get_value("items/0/lettering_type"); ?>
        <div id="items_0_lettering_type" class="clearfix items_error <?php if (has_error("items/0/lettering_type")) echo "error"; ?>">
            <label class="swrt2">Нужна услуга по нанесению надписи на изделие? <span class="red-str">*стоимость услуги 150 р.</span></label>
            <div class="col-md-2 col-xs-12">
                <input type="radio" id="items_0_lettering_type_no" name="items[0][lettering_type]" value="no"
                    <?php if ($value == "no") echo "checked"; ?> class="lettering_type">
                <span>Нет</span>
            </div>
            <div class="col-md-3 col-xs-12">
                <input type="radio" id="items_0_lettering_type_black" name="items[0][lettering_type]" value="black"
                    <?php if ($value == "black") echo "checked"; ?> class="lettering_type">
                <span>Да (черная)</span>
            </div>

            <div class="col-md-5 col-xs-12">
                <input type="radio" id="items_0_lettering_type_golden" name="items[0][lettering_type]" value="golden"
                    <?php if ($value == "golden") echo "checked"; ?> class="lettering_type">
                <span>Да (золотистая)</span>
            </div>

            <div class="col-md-5 col-xs-12 lettering_type_blue">
                <input type="radio" id="items_0_lettering_type_blue" name="items[0][lettering_type]" value="blue"
                    <?php if ($value == "blue") echo "checked"; ?> class="lettering_type">
                <span>Да (синяя)</span>
            </div>
        </div>

        <?php $value = get_value("items/0/lettering_option"); ?>
        <label>
            Выберите надпись или оставьте свою (не более 40 символов)
        </label>
        <select name="items[0][lettering_option]"
                class="formname lettering_option <?php if (has_error("items/0/lettering_option")) echo "error"; ?>">
            <option value="Свой вариант" <?php if ($value == "Свой вариант") echo "selected"; ?>>
                Свой вариант
            </option>
            <?php foreach ($templates as $template): ?>
                <option value="<?php echo $template; ?>" <?php if ($value == $template) echo "selected"; ?>>
                    <?php echo $template; ?></option>
            <?php endforeach; ?>
        </select>

        <?php $value = get_value("items/0/lettering"); ?>
        <input maxlength="40" type="text" id="items_0_lettering" class="formname items_error <?php if (has_error("items/0/lettering")) echo "error"; ?>"
               name="items[0][lettering]"
               value="<?php echo $value; ?>" <?php if (get_value("items/0/lettering_option") == "Свой вариант") echo "style=\"display:none;\""; ?>/>

        <?php $value = get_value("delivery"); ?>
        <div class="clearfix">
            <label class="spo1 lbb1">
                Выберите способ доставки
                <span>*</span>
            </label>
            <div id="delivery" class="clearfix items_error <?php if (has_error("delivery")) echo "error"; ?>">
                <div class="col-md-4 col-sm-12 tpx-padd tpx-padds">
                    <input type="radio" id="delivery_courier" name="delivery" value="courier"
                        <?php if ($value == "courier") echo "checked"; ?>>
                    <span>Курьерская доставка</span>
                </div>
                <div class="col-md-4 col-sm-12 tpx-padd">
                    <input type="radio" id="delivery_pickup" name="delivery" value="pickup"
                        <?php if ($value == "pickup") echo "checked"; ?>>
                    <span>Самовывоз</span>
                </div>
                <div class="col-md-4 col-sm-12 tpx-padd">
                    <input type="radio" id="delivery_post" name="delivery" value="post"
                        <?php if ($value == "post") echo "checked"; ?>>
                    <span>Почта России</span>
                </div>
                <p class="spors clearfix">
                    Ваш заказ можно забрать по адресу: г. Москва, Малый Ивановский пер., 11/6, стр. 1
                </p>

                <?php $value = get_value("city"); ?>
                <label class="address">
                    Введите город
                    <span>*</span>
                </label>
                <input id="city" type="text" name="city" class="address formname items_error <?php if (has_error("city")) echo "error"; ?>"
                       size="40" placeholder="Город" value="<?php echo $value; ?>">

                <?php $value = get_value("street"); ?>
                <label class="address">
                    Введите улицу
                    <span>*</span>
                </label>
                <input id="street" type="text" name="street" class="address formname items_error <?php if (has_error("street")) echo "error"; ?>"
                       size="40" placeholder="Улица" value="<?php echo $value; ?>">

                <?php $value = get_value("house"); ?>
                <label class="address">
                    Введите номер дома
                </label>
                <input id="house" type="text" name="house" class="address formname items_error <?php if (has_error("house")) echo "error"; ?>"
                       size="40" placeholder="Номер дома" value="<?php echo $value; ?>">

                <?php $value = get_value("housing"); ?>
                <label class="address">
                    Введите номер корпуса
                </label>
                <input id="housing" type="text" name="housing" class="address formname items_error <?php if (has_error("housing")) echo "error"; ?>"
                       size="40" placeholder="Номер корпуса" value="<?php echo $value; ?>">

                <?php $value = get_value("build"); ?>
                <label class="address">
                    Введите номер строения
                </label>
                <input id="build" type="text" name="build" class="address formname items_error <?php if (has_error("build")) echo "error"; ?>"
                       size="40" placeholder="Номер строения" value="<?php echo $value; ?>">

                <?php $value = get_value("flat"); ?>
                <label class="address">
                    Введите номер квартиры
                </label>
                <input id="flat" type="text" name="flat" class="address formname items_error <?php if (has_error("flat")) echo "error"; ?>"
                       size="40" placeholder="Номер квартиры" value="<?php echo $value; ?>">

                <?php $value = get_value("index"); ?>
                <label class="address">
                    Введите индекс
                </label>
                <input id="index" type="text" name="index" class="address formname items_error <?php if (has_error("index")) echo "error"; ?>"
                       size="40" placeholder="Индекс" value="<?php echo $value; ?>">
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">

        <?php $value = get_value("first_name"); ?>
        <label>
            Введите имя
            <span>*</span>
        </label>
        <input id="first_name" type="text" name="first_name" class="formname items_error <?php if (has_error("first_name")) echo "error"; ?>"
               size="40" placeholder="Имя" value="<?php echo $value; ?>">

        <?php $value = get_value("last_name"); ?>
        <label>Введите фамилию</label>
        <input id="last_name" type="text" name="last_name" class="formname items_error <?php if (has_error("last_name")) echo "error"; ?>"
               size="40" placeholder="Фамилия" value="<?php echo $value; ?>">

        <?php $value = get_value("phone"); ?>
        <label>
            Введите телефон
            <span>*</span>
        </label>
        <input id="phone" type="tel" name="phone" class="formname items_error <?php if (has_error("phone")) echo "error"; ?>"
               size="40" placeholder="Телефон" value="<?php echo $value; ?>">

        <?php $value = get_value("email"); ?>
        <label>
            Введите электронную почту
            <span>*</span>
        </label>
        <input id="email" type="email" name="email" class="formname items_error <?php if (has_error("email")) echo "error"; ?>"
               size="40" placeholder="E-mail" value="<?php echo $value; ?>">

        <?php $value = get_value("info"); ?>
        <label>Уточнения по заказу</label>
        <textarea id="info" rows="10" name="info" class="formtext items_error <?php if (has_error("info")) echo "error"; ?>" cols="30"
                          placeholder="Сообщение"><?php echo $value; ?></textarea>

        <label class="ndt">
            Прикрепите изображение для печати (не менее 150 dpi)
            <span>*</span>
        </label>
        <input name="attachments[0]" type="file" size="28" <?php if (has_error("attachments/0")) echo "class=\"error\""; ?>>
        <div class="btn-sub disb" onclick="repairIndices(); clone();">+ Добавить футболку</div>
    </div>

    <?php for ($index = 1; $index < (int)get_value("items"); $index++): ?>
        <div class="auxiliary-form">
            <h3>Футболка <span class="index"><?php echo $index + 1; ?></span></h3>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">

                        <?php $value = get_value("items/$index/type"); ?>
                        <div class="clearfix">
                            <label class="spo1">
                                Выберите тип
                                <span>*</span>
                            </label>
                            <div id="items_<?php echo $index; ?>_type" class="clearfix items_error <?php if (has_error("items/$index/type")) echo "error"; ?>">
                                <div class="col-md-2 mtdx">
                                    <input type="radio" id="items_<?php echo $index; ?>_type_male"
                                           name="items[<?php echo $index; ?>][type]" value="male" class="type"
                                        <?php if ($value == "male") echo "checked"; ?>>
                                    <span>Мужская</span>
                                </div>
                                <div class="col-md-3 mtdx">
                                    <input type="radio" id="items_<?php echo $index; ?>_type_female"
                                           name="items[<?php echo $index; ?>][type]" value="female" class="type"
                                        <?php if ($value == "female") echo "checked"; ?>>
                                    <span>Женская</span>
                                </div>
                                <div class="col-md-2 mtdx mx-xv">
                                    <input type="radio" id="items_<?php echo $index; ?>_type_child"
                                           name="items[<?php echo $index; ?>][type]" value="child" class="type"
                                        <?php if ($value == "child") echo "checked"; ?>>
                                    <span>Детская</span>
                                </div>
                            </div>
                        </div>

                        <?php $value = get_value("items/$index/size"); ?>
                        <div class="clearfix mtsize">
                            <label class="swrt">
                                Укажите желаемый размер футболки
                                <span>*</span>
                                <a class="tbl hidden-xs" data-toggle="modal" href="#" data-target="#largeModal">
                                    (Таблица
                                    размеров)
                                </a>
                            </label>
                            <select id="items_<?php echo $index; ?>_size" name="items[<?php echo $index; ?>][size]" data-default="" class="items_error <?php if (has_error("items/$index/size")) echo "error"; ?>">
                                <option disabled <?php if (!$value) echo "selected"; ?> value class="male female child">
                                    Выберите
                                </option>
                                <?php foreach ($sizes as $size => $types): ?>
                                    <option value="<?php echo $size; ?>" <?php if ($value == $size) echo "selected"; ?>
                                            class="<?php echo implode(" ", $types); ?>">
                                        <?php echo $size; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <?php $value = get_value("items/$index/lettering_type"); ?>
                        <div id="items_<?php echo $index; ?>_lettering_type" class="clearfix items_error <?php if (has_error("items/$index/lettering_type")) echo "error"; ?>">
                            <label class="swrt2">Нужна услуга по нанесению надписи на изделие?<span class="red-str">*стоимость услуги 150 р.</span></label>
                            <div class="col-md-2 col-xs-12">
                                <input type="radio" id="items_<?php echo $index; ?>_lettering_type_no"
                                       name="items[<?php echo $index; ?>][lettering_type]" value="no"
                                    <?php if ($value == "no") echo "checked"; ?>
                                       class="lettering_type">
                                <span>Нет</span>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <input type="radio" id="items_<?php echo $index; ?>_lettering_type_black"
                                       name="items[<?php echo $index; ?>][lettering_type]" value="black"
                                    <?php if ($value == "black") echo "checked"; ?>
                                       class="lettering_type">
                                <span>Да (черная)</span>
                            </div>
                            <div class="col-md-5 col-xs-12">
                                <input type="radio" id="items_<?php echo $index; ?>_lettering_type_golden"
                                       name="items[<?php echo $index; ?>][lettering_type]" value="golden"
                                    <?php if ($value == "golden") echo "checked"; ?>
                                       class="lettering_type">
                                <span>Да (золотистая)</span>
                            </div>
                            <div class="col-md-5 col-xs-12 lettering_type_blue">
                                <input type="radio" id="items_<?php echo $index; ?>_lettering_type_blue"
                                       name="items[<?php echo $index; ?>][lettering_type]" value="blue"
                                    <?php if ($value == "blue") echo "checked"; ?>
                                       class="lettering_type">
                                <span>Да (синяя)</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12">

                        <?php $value = get_value("items/$index/lettering_option"); ?>
                        <label>
                            Выберите надпись или оставьте свою (не более 40 символов)
                        </label>
                        <select id="items_<?php echo $index; ?>_lettering_option" name="items[<?php echo $index; ?>][lettering_option]" class="formname lettering_option items_error <?php if (has_error("items/$index/lettering_option")) echo "error"; ?>">
                            <option value="Свой вариант" <?php if ($value == "Свой вариант") echo "selected"; ?>>
                                Свой вариант
                            </option>
                            <?php foreach ($templates as $template): ?>
                                <option
                                    value="<?php echo $template; ?>" <?php if ($value == $template) echo "selected"; ?>>
                                    <?php echo $template; ?></option>
                            <?php endforeach; ?>
                        </select>

                        <?php $value = get_value("items/$index/lettering"); ?>
                        <input type="text" id="items_<?php echo $index; ?>_lettering"
                               class="formname items_error <?php if (has_error("items/$index/lettering")) echo "error"; ?>"
                               name="items[<?php echo $index; ?>][lettering]"
                               value="<?php echo $value; ?>" <?php if (get_value("items/$index/lettering_option") == "Свой вариант") echo "style=\"display:none;\""; ?>/>

                        <label class="ndt">
                            Прикрепите изображение для печати (не менее 150 dpi)
                            <span>*</span>
                        </label>
                        <input name="attachments[<?php echo $index; ?>]" type="file"
                               size="28" <?php if (has_error("attachments/$index")) echo "class=\"error\""; ?>>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="btn-sub disbs"
                                 onclick="$(this).parents('.auxiliary-form').remove(); repairIndices();">Скрыть блок
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="btn-sub disb" onclick="repairIndices(); clone();">+ Добавить футболку</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endfor; ?>

    <div class="auxiliary-form template" style="display: none">
        <h3>Футболка <span class="index"></span></h3>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">

                    <div class="clearfix">
                        <label class="spo1">
                            Выберите тип
                            <span>*</span>
                        </label>
                        <div id="items_$i_type" class="clearfix items_error">
                            <div class="col-md-2 mtdx">
                                <input type="radio" name="type" value="male" class="type" checked disabled>
                                <span>Мужская</span>
                            </div>
                            <div class="col-md-3 mtdx">
                                <input type="radio" name="type" value="female" class="type" disabled>
                                <span>Женская</span>
                            </div>
                            <div class="col-md-2 mtdx mx-xv">
                                <input type="radio" name="type" value="child" class="type" disabled>
                                <span>Детская</span>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix mtsize">
                        <label class="swrt">
                            Укажите желаемый размер футболки
                            <span>*</span>
                            <a class="tbl hidden-xs" data-toggle="modal" href="#" data-target="#largeModal">
                                (Таблица
                                размеров)
                            </a>
                        </label>
                        <select id="items_$i_size" name="size" data-default="" disabled class="items_error">
                            <option disabled selected value class="male female child">Выберите</option>
                            <?php foreach ($sizes as $size => $types): ?>
                                <option value="<?php echo $size; ?>" class="<?php echo implode(" ", $types); ?>">
                                    <?php echo $size; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div id="items_$i_lettering_type" class="clearfix items_error">
                        <label class="swrt2">Нужна услуга по нанесению надписи на изделие?<span class="red-str">*стоимость услуги 150 р.</span></label>
                        <div class="col-md-2 col-xs-12">
                            <input type="radio" name="lettering_type" value="no" checked disabled
                                   class="lettering_type">
                            <span>Нет</span>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <input type="radio" name="lettering_type" value="black" disabled class="lettering_type">
                            <span>Да (черная)</span>
                        </div>
                        <div class="col-md-5 col-xs-12">
                            <input type="radio" name="lettering_type" value="golden" disabled class="lettering_type">
                            <span>Да (золотистая)</span>
                        </div>
                        <div class="col-md-5 col-xs-12 lettering_type_blue">
                            <input type="radio" name="lettering_type" value="blue" disabled class="lettering_type">
                            <span>Да (синяя)</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">

                    <label>
                        Выберите надпись или оставьте свою (не более 40 символов)
                    </label>
                    <select id="items_$i_lettering_option" name="lettering_option" class="formname lettering_option items_error" disabled>
                        <option value="Свой вариант" selected>
                            Свой вариант
                        </option>
                        <?php foreach ($templates as $template): ?>
                            <option value="<?php echo $template; ?>">
                                <?php echo $template; ?></option>
                        <?php endforeach; ?>
                    </select>

                    <input id="items_$i_lettering" type="text" class="formname items_error" disabled
                           name="lettering" style="display:none;"/>

                    <label class="ndt">
                        Прикрепите изображение для печати (не менее 150 dpi)
                        <span>*</span>
                    </label>
                    <input name="attachments" type="file" disabled size="28">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="btn-sub disbs"
                             onclick="$(this).parents('.auxiliary-form').remove(); repairIndices();">Скрыть блок
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="btn-sub disb" onclick="repairIndices(); clone();">+ Добавить футболку</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="flr col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                <?php $value = get_value("payment"); ?>
                <div class="clearfix">
                    <label class="spo2">
                        Выберите способ оплаты
                        <span>*</span>
                    </label>
                    <div id="payment" class="clearfix items_error <?php if (has_error("payment")) echo "error"; ?>">
                        <div class="col-md-6 col-sm-12 tpx-padd">
                            <input type="radio" id="payment_cash" name="payment" value="cash"
                                <?php if ($value == "cash") echo "checked"; ?>>
                            <span>Наличный расчет</span>
                        </div>
                        <div class="col-md-6 col-sm-12 tpx-padd">
                            <input type="radio" id="payment_credit" name="payment" value="credit"
                                <?php if ($value == "credit") echo "checked"; ?>>
                            <span>Безналичный расчет</span>
                        </div>
                    </div>
                </div>

                <label class="ndt gtr">* Срок изготовления футболки от двух дней<br> *При покупке от двух и более
                    футболок действуют скидки</label>
                <div class="price">
                    <strong>Цена:</strong>
                            <span>
                                <span class="price_value">1000</span>
                                руб.
                            </span>
                </div>
                <button class="btn-sub" type="submit">Оплатить</button>
            </div>
        </div>
    </div>
</form>