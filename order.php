<?php defined('INDIRECT_ACCESS') or define('INDIRECT_ACCESS', true);
require_once "helper.php";
require_once "query.php";
require_once "mailer.php";
require_once "delivery.php";

session_destroy();
session_start();

set_value('init', false);

$attachments = $_FILES['attachments'];
$items = $_POST['items'];
$out_sum = 0;
foreach ($items as $i => $item){
    set_value("items/$i/type", $item['type'], in_array($item['type'], ['male', 'female', 'child']));
    set_value("items/$i/size", $item['size'], in_array($item['size'], array_keys($sizes)) && (!in_array($item['type'], ['male', 'female', 'child']) || in_array($item['type'], $sizes[$item['size']])));
    set_value("items/$i/lettering_type", $item['lettering_type'], in_array($item['lettering_type'], ['no', 'black', 'golden', 'blue']));
    $lo = !$item['lettering_type'] || $item['lettering_type'] == "no" || $item['lettering_option'];
    set_value("items/$i/lettering_option", $item['lettering_option'], $lo);
    $l = !$item['lettering_type'] || $item['lettering_type'] == "no" || !$item['lettering_option'] || $item['lettering_option'] != "Свой вариант" || $item['lettering'];
    set_value("items/$i/lettering", $item['lettering'], $l);

    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    set_value("attachments/$i", "",
        $attachments && $attachments['error'][$i] == 0 && in_array(finfo_file($finfo, $attachments['tmp_name'][$i]), $mime_types));

    $out_sum += $prices[$item['type']];
    if($item['lettering_type'] != "no") $out_sum += $prices['lettering'];
}
set_value("items", count($items));

switch(count($items)){
    case 0:
    case 1:
        break;
    case 2:
    case 3:
        $out_sum -= 100 * count($items);
        break;
    case 4:
        $out_sum -= 200 * count($items);
        break;
    default:
        $out_sum -= 300 * count($items);
}

$first_name = $_POST['first_name']; // required
set_value("first_name", $first_name, !!$first_name);

$last_name = $_POST['last_name'];
set_value("last_name", $last_name);

$phone = $_POST['phone']; // required && phone
set_value("phone", $phone, !!$phone);

$email = $_POST['email']; // required && email
set_value("email", $email, !!$email && preg_match("/[0-9a-z_]+@[0-9a-z_^\.]+\.[a-z]{2,3}/i", $email));

$info = $_POST['info'];
set_value("info", $info);

$delivery = $_POST['delivery']; // required && courier|pickup|post
set_value("delivery", $delivery, in_array($delivery, ['courier', 'pickup', 'post']));

$city = $_POST['city'];
set_value("city", $city, !!$city || $delivery == 'pickup');

$street = $_POST['street'];
set_value("street", $street, !!$street || $delivery == 'pickup');

$house = $_POST['house'];
set_value("house", $house);

$housing = $_POST['housing'];
set_value("housing", $housing);

$build = $_POST['build'];
set_value("build", $build);

$flat = $_POST['flat'];
set_value("flat", $flat);

$index = $_POST['index'];
set_value("index", $index);

$payment = $_POST['payment']; // required && cash|credit
set_value("payment", $payment, in_array($payment, ['cash', 'credit']));

if(has_errors())
    $_index();

$data = [
    'first_name' => $first_name,
    'last_name' => $last_name,
    'email' => $email,
    'phone' => $phone,
    'info' => $info,
    'delivery' => $delivery,
    'address' => "г.$city, ул.$street, д.$house, корп.$housing, стр.$build, кв.$flat, $index",
    'sum' => $out_sum,
    'payment' => $payment
];

$data['inv_id'] = $inv_id = $createOrder($data);
$data['items'] = $items;
$data['attachments'] = $attachments;

sendMail($data);
if($delivery != 'pickup') {
    $data['city'] = $city;
    $data['street'] = $street;
    $data['house'] = $house;
    $data['housing'] = $housing;
    $data['build'] = $build;
    $data['flat'] = $flat;
    $data['index'] = $index;
    Delivery::createOrder($data);
}

session_destroy();
session_start();

require_once 'kassa.php';
