<?php if (!defined('INDIRECT_ACCESS') || !INDIRECT_ACCESS) die('No direct access allowed.');
require_once 'php_mailer.php';

function sendMail($data){
    $inv_id = $data['inv_id'];
    $first_name = substr(htmlspecialchars(trim($data['first_name'])), 0, 1000000);
    $last_name = substr(htmlspecialchars(trim($data['last_name'])), 0, 1000000);
    $email = substr(htmlspecialchars(trim($data['email'])), 0, 1000000);
    $phone = substr(htmlspecialchars(trim($data['phone'])), 0, 1000000);
    $info = substr(htmlspecialchars(trim($data['info'])), 0, 1000000);
    $delivery = $data['delivery'] == "courier" ? "курьер" : ($data['delivery'] == "pickup" ? "самовывоз" : "Почта России");
    $address = substr(htmlspecialchars(trim($data['address'])), 0, 1000000);
    $sum = $data['sum'];
    $payment = $data['payment'] == "cash" ? "наличные" : "РобоКасса";

    $mailer = new PHPMailer();
    $mailer->CharSet = "UTF-8";
    $date = date("d.m.Y | h:i");
    $mailer->Subject = "Заказ #$inv_id $date $last_name $first_name ";  // тема письма
    $mailer->From = "podaripobedy@1gb.ru"; // адрес, от кого письмо
    $mailer->FromName = "Роспатриот"; // подпись от кого
    $mailer->ReplyTo = $email;
    $mailer->AddAddress('info@podaripobedy.ru', 'Отдел продаж'); // Указываем свой e-mail
    $mailer->IsHTML(true); // Возможность вставки в письмо html

    $body = "
<b>Имя:</b> $first_name<br/>
<b>Фамилия:</b> $last_name<br/>
<b>Телефон:</b> $phone<br/>
<b>Адрес электронной почты:</b> $email<br/>
<b>Уточнения по заказу:</b> $info<br/>
<br/>
<b>Состав заказа:</b><br/><br/>";

    $attachments = $data['attachments'];
    foreach($data['items'] as $i => $item){
        $size = substr(htmlspecialchars(trim($item['size'])), 0, 1000000);
        $lettering_type = $item['lettering_type'] == "black" ? "чёрный" : ($data['lettering_type'] == "golden" ? "золотистый" : "нет");
        $lettering = $item['lettering_option'];
        if($lettering == "Свой вариант")$lettering = $item['lettering'];
        $lettering = substr(htmlspecialchars(trim($lettering)), 0, 1000000);
        $type = $item['type'] == "male" ? "мужская" : ($item['type'] == "female" ? "женская" : "детская");
        $attachment = $attachments['name'][$i];

        $body .= "
<b>Размер:</b> $size<br/>
<b>Цвет нанесения:</b> $lettering_type<br/>
<b>Текст нанесения:</b> $lettering<br/>
<b>Тип футболки:</b> $type<br/>
<b>Название файла:</b> $attachment<br/>
<br/>
<br/>
";
        $mailer->AddAttachment($attachments['tmp_name'][$i], $attachment);
    }

    $mailer->Body = "
$body
<b>Способ доставки:</b> $delivery<br/>
<b>Адрес доставки:</b> $address<br/>
<b>Сумма платежа:</b> $sum<br/>
<b>Способ оплаты:</b> $payment<br/>
";

    if (!$mailer->Send()) die ('Mailer Error: ' . $mailer->ErrorInfo);
}

/* function sendMail2($data){
    $inv_id = $data['inv_id'];
    $first_name = substr(htmlspecialchars(trim($data['first_name'])), 0, 1000000);
    $last_name = substr(htmlspecialchars(trim($data['last_name'])), 0, 1000000);
    $email = substr(htmlspecialchars(trim($data['email'])), 0, 1000000);
    $phone = substr(htmlspecialchars(trim($data['phone'])), 0, 1000000);
    $info = substr(htmlspecialchars(trim($data['info'])), 0, 1000000);
    $address = substr(htmlspecialchars(trim($data['address'])), 0, 1000000);

    $mailer = new PHPMailer();
    $mailer->CharSet = "UTF-8";
    $date = date("d.m.Y | h:i");
    $mailer->Subject = "Заказ #$inv_id $date $last_name $first_name ";  // тема письма
    $mailer->setFrom("info@podaripobedy.ru", "Роспатриот"); // адрес, от кого письмо
    $mailer->addReplyTo($email);
    $mailer->AddAddress('info@podaripobedy.ru', 'Отдел информации'); // Указываем свой e-mail
    $mailer->IsHTML(true); // Возможность вставки в письмо html

    $mailer->Body = "
<b>Имя:</b> $first_name<br/>
<b>Фамилия:</b> $last_name<br/>
<b>Телефон:</b> $phone<br/>
<b>Адрес электронной почты:</b> $email<br/>
<b>Уточнения по заказу:</b> $info<br/>
<br/>
<b>Адрес доставки:</b> $address<br/>";

    $mailer->AddAttachment($data['attachment']['tmp_name'], $data['attachment']['name']);
    if (!$mailer->Send()) die ('Mailer Error: ' . $mailer->ErrorInfo);
} */
