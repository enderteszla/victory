<?php if(!defined('INDIRECT_ACCESS') || !INDIRECT_ACCESS) die('No direct access allowed.');
require_once 'helper.php';

$link = "https://money.yandex.ru/eshop.xml";

if($data['payment'] == 'cash'){
    set_value('message', "Заказ {$data['inv_id']} успешно создан.");
    $_index();
}

$shopId = "58561";
$scid = "54014";
$sum = $data['sum'];
$customerNumber = $data['email'];
$orderNumber = $data['inv_id'];
$cps_email = $data['email'];
$cps_phone = $data['phone'];

$link .= "?shopId=$shopId&scid=$scid&sum=$sum&customerNumber=$customerNumber&orderNumber=$orderNumber&cps_phone=$cps_phone&cps_email=$cps_email";

header("Location: $link");
exit();
