﻿<?php defined('INDIRECT_ACCESS') or define('INDIRECT_ACCESS', true);
require_once "helper.php";

$inv_id = $checkInvoice();
if ($inv_id == -1)
    set_value('message', "Некорректная подпись платежа.");
else {
    $status = $isCashPayment() ? "создан" : "оплачен";
    set_value('message', "Заказ $inv_id успешно $status.");
}

$_index();