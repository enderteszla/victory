<?php if(!defined('INDIRECT_ACCESS') || !INDIRECT_ACCESS) die('No direct access allowed.');
require_once "private/robokassa_test.php";

// тип товара
// code of goods
# $shp_item = "2";

if ($payment === "cash") {
    # $crc  = md5("$out_sum:$inv_id:$mrh_pass1:Shp_item=$shp_item");
    $crc = md5("$out_sum:$inv_id:$mrh_pass1");
    $_success(['OutSum' => $out_sum, 'InvId' => $inv_id, 'SignatureValue' => $crc, 'cash' => true]);
}

// описание заказа
// order description
$inv_desc = "Роспатриот -- Футболки \"Подари Победу!\"";

// предлагаемая валюта платежа
// default payment e-currency
$in_curr = "";

// язык
// language
$culture = "ru";

// формирование подписи
// generate signature
# $crc  = md5("$mrh_login:$out_sum:$inv_id:$mrh_pass1:Shp_item=$shp_item");
$crc = md5("$mrh_login:$out_sum:$inv_id:$mrh_pass1");

$fields = [
    'MrchLogin' => $mrh_login,
    'OutSum' => $out_sum,
    'InvId' => $inv_id,
    'Desc' => $inv_desc,
    'SignatureValue' => $crc,
    # 'Shp_item' => $shp_item,
    'IncCurrLabel' => $in_curr,
    'Culture' => $culture
];

$fields['IsTest'] = 1;

$link = "https://merchant.roboxchange.com/Index.aspx?" . implode("&", array_map(function($k,$v){
        return "$k=$v";
    }, array_keys($fields), array_values($fields)));

header("Location: $link");