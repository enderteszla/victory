<?php if(!defined('INDIRECT_ACCESS') || !INDIRECT_ACCESS) die('No direct access allowed.');
$prices = [
    'male' => 1500,
    'female' => 1500,
    'child' => 1400,
    'lettering' => 150,
];
$sizes = [
    "24-26" => ['child'],
    "28-30" => ['child'],
    "30-32" => ['child'],
    "32-34" => ['child'],
    "34-36" => ['child'],
    "36-38" => ['child'],
    "38-40" => ['child'],
    "40-42" => ['female','child'],
    "42-44" => ['female'],
    "44-46" => ['male','female'],
    "46-48" => ['male','female'],
    "48-50" => ['male','female'],
    "50-52" => ['male','female'],
    "52-54" => ['male'],
    "54-56" => ['male'],
];
$mime_types = [
    "image/jpeg",
    "image/bmp",
    "image/gif",
    "image/png",
    "image/tiff",
];
$templates = [
    "Мы победили!",
    "Слава героям!",
    "Будем помнить!",
    "За Родину!",
    "Горжусь подвигом!",
    "Вечная слава!",
    "Берлин наш!",
    "Мой дед!",
    "9 мая - Великий праздник!",
    "Спасибо деду, за победу!"
];