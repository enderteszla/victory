jQuery(function($){
    setInterval(function(){
        var price = 0,
            count = $('.type:checked').not(':disabled').each(function(){
                var index = /items\[(\d+)\]\[type\]/.exec($(this).attr('name'))[1],
                    type = $(this).val();
                price += prices[type];
                if($("[name=\"items[" + index + "][lettering_type]\"]:checked").val() != 'no') price += prices['lettering'];
            }).length,
            discount;
        
        switch(count){
            case 0:
            case 1:
                discount = 0;
                break;
            case 2:
            case 3:
                discount = count * 100;
                break;
            case 4:
                discount = count * 200;
                break;
            default:
                discount = count * 300;
        }
        
        $('span.price_value').text(price - discount);
        if($('#delivery_pickup').is(':checked')){
            $('.spors').show();
            $('.address').hide();
        } else {
            $('.spors').hide();
            $('.address').show();
        }
        if($('#payment_cash').is(':checked')){
            $('button[type="submit"]').text("Заказать");
        } else {
            $('button[type="submit"]').text("Оставить заявку");
        }
    }, 500);
    $(document).delegate('.lettering_option', 'change', function(e){
        if(!$(e.target).is(':disabled')) {
            var ex = /items\[(\d+)\]\[lettering_option\]/.exec($(e.target).attr('name')),
                index = ex ? ex[1] : 0,
                lettering_field = $("[name=\"items[" + index + "][lettering]\"]");
            if ($(e.target).val() == "Свой вариант")
                lettering_field.show();
            else
                lettering_field.hide();
        }
    }).delegate('.lettering_type', 'change', function(e){
        if(!$(e.target).is(':disabled')) {
            var ex = /items\[(\d+)\]\[lettering_type\]/.exec($(e.target).attr('name')),
                index = ex ? ex[1] : 0,
                lettering_option_field = $("[name=\"items[" + index + "][lettering_option]\"]").trigger('change');
            if ($(e.target).val() == "no") {
                lettering_option_field.styler('destroy').hide().prev('label').hide();
                $("[name=\"items[" + index + "][lettering]\"]").hide();
            } else
                lettering_option_field.styler({selectSearch: true}).show().parents('.lettering_option').prev('label').show();
        }
    }).delegate('.type', 'change', function(e){
        if(!$(e.target).is(':disabled')) {
            if ($(e.target).is(':checked') && !$(e.target).is(':disabled')) {
                var ex = /items\[(\d+)\]\[type\]/.exec($(e.target).attr('name')),
                    index = ex ? ex[1] : 0,
                    select = $("[name=\"items[" + index + "][size]\"]"),
                    type = $(e.target).val();
                select.styler('destroy').children('option').hide().attr('disabled', 'disabled').filter('.' + type).show().removeAttr('disabled');
                select.styler({selectSearch: true});
            }
        }
    }).delegate('form', 'submit', function(e){
        if($(this).data('valid')) return true;
        e.preventDefault();
        $.post("/check.php", $(this).serializeArray(), function(data){
            $('.items_error').removeClass('error');
            for(var key in data)
                if(!data[key])
                    $('#' + key).addClass('error');
            if($('.items_error').filter('.error').length == 0){
                $(e.target).data('valid', true).submit();
            }
        }, 'json');
    });
    $('.lettering_option').trigger('change');
    $('.lettering_type').trigger('change');
    $('.type').trigger('change');
});

function repairIndices(){
    $('.auxiliary-form').not('.template').each(function(i, form){
        var field_id_reg = /^items_\d+_(.*)$/;
        $(form).find('span.index').text(++i + 1);
        $(form).find('input,select').styler('destroy').each(function(){
            var id = $(this).attr('id'),
                name = $(this).attr('name'),
                attachment_reg = /^attachments\[\d+\]$/,
                field_name_reg = /^items\[\d+\]\[(.*)\]$/;
            if(attachment_reg.test(name))
                $(this).attr('name', 'attachments[' + i + ']');
            else if(field_name_reg.test(name))
                $(this).attr('name', 'items[' + i + '][' + field_name_reg.exec(name)[1] + ']');
            if(field_id_reg.test(id))
                $(this).attr('id', 'items_' + i + '_' + field_id_reg.exec(id)[1]);
        }).styler({selectSearch: true});
        $(form).find('.items_error').each(function(){
            $(this).attr('id', 'items_' + i + '_' + field_id_reg.exec($(this).attr('id'))[1]);
        });
    });
}

function clone(){
    repairIndices();
    var index = $('.auxiliary-form').not('.template').length + 1,
        template = $('.auxiliary-form.template'),
        form = template.clone().show().removeClass('template').insertBefore(template);
    form.find('span.index').text(index + 1);
    form.find('input,select').removeAttr('disabled').each(function(){
        var name = $(this).attr('name');
        if(name == 'attachments')
            $(this).attr('name', 'attachments[' + index + ']');
        else
            $(this).attr('name', 'items[' + index + '][' + name + ']');
        if(name == 'type' || name == 'size' || name == 'lettering_type') {
            if ($(this).is('select'))
                $(this).attr('id', 'items_' + index + '_' + name);
            else
                $(this).attr('id', 'items_' + index + '_' + name + '_' + $(this).val());
        }
    }).styler({selectSearch: true});
    form.find('.items_error').each(function(){
        $(this).attr('id', $(this).attr('id').replace('$i', index));
    });
    form.find('.lettering_option').trigger('change');
    form.find('.lettering_type').trigger('change');
    form.find('.type').trigger('change');
}