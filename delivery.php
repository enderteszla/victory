<?php if(!defined('INDIRECT_ACCESS') || !INDIRECT_ACCESS) die('No direct access allowed.');

class Delivery {
    static function sort($fields){
        $keys = array_keys($fields);
        sort($keys);
        var_export($keys);
        return array_map(function($key)use($fields){
            return $fields[$key];
        }, $keys);
    }

    static function sign($fields){
        $sorted_fields = self::sort($fields);
        var_export($sorted_fields);
        return md5(implode("", $sorted_fields) . "8e6534c69ef58c85898f3745982c21a6f530f6f0fdb76e4f7c22bba52b1a5608");
    }

    static function createOrder($data){
        $fields = [
            'client_id' => 10807,
            'sender_id' => 9109,
            'recipient_first_name' => $data['first_name'],
            'recipient_last_name' => $data['last_name'],
            'recipient_phone' => $data['phone'],
            'recipient_email' => $data['email'],
            'order_num' => $data['inv_id'],
            'order_assessed_value' => $data['sum'],
            'deliverypoint_city' => $data['city'],
            'deliverypoint_street' => $data['street'],
            'deliverypoint_house' => $data['house'],
            'deliverypoint_housing' => $data['housing'],
            'deliverypoint_build' => $data['build'],
            'deliverypoint_flat' => $data['flat'],
            'deliverypoint_index' => $data['index']
        ];
        foreach($data['items'] as $i => $item){
            $j = $i + 1;
            $article = $item['size'];
            switch($item['type']){
                case 'child':
                    $article = "дет.$article";
                    break;
                case 'female':
                    $article = "жен.$article";
                    break;
                case 'male':
                    $article = "муж.$article";
                    break;
            }
            if ($item['letering_type'] == 'no')
                $name = "Без надписи";
            else {
                $lettering = $item['lettering_option'] ? $item['lettering_option'] : $item['lettering'];
                $name = " надпись \"$lettering\"";
                switch($item['lettering_type']){
                    case 'black':
                        $name = "Чёрная$name";
                        break;
                    case 'golden':
                        $name = "Золотистая$name";
                        break;
                    case 'blue':
                        $name = "Синяя$name";
                        break;
                }
            }

            $fields["order_items[$j][orderitem_article]"] = $article;
            $fields["order_items[$j][orderitem_name]"] = $name;
            $fields["order_items[$j][orderitem_quantity]"] = 1;
            $fields["order_items[$j][orderitem_cost]"] = $data['sum'] / count($data['items']);
        }
        $fields['secret_key'] = self::sign($fields);
        $query = implode("&", array_map(function($k,$v){return "$k=$v";}, array_keys($fields), array_values($fields)));
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://delivery.yandex.ru/api/1.0/createOrder');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $query);
        $log_file = fopen("log/{$data['inv_id']}.json", "wb");
        fwrite($log_file, curl_exec($curl));
        curl_close($curl);
    }
}