<?php

class Session
{
    public static function set_value($key, $value, $valid = true)
    {
        $_SESSION[$key] = ['value' => $value, 'valid' => $valid];
    }

    public static function get_value($key, $default = "")
    {
        return array_key_exists($key, $_SESSION) ? $_SESSION[$key]['value'] : $default;
    }

    public static function has_error($key)
    {
        return !(self::get_value('init', true) || array_key_exists($key, $_SESSION) && $_SESSION[$key]['valid']);
    }

    public static function has_errors()
    {
        return count(self::errors()) > 0;
    }

    public static function errors(){
        $errors = [];
        foreach ($_SESSION as $key => $element) {
            if (is_array($element) && array_key_exists('valid', $element) && !$element['valid'])
                $errors[] = $key;
        }
        return $errors;
    }
};