<?php if(!defined('INDIRECT_ACCESS') || !INDIRECT_ACCESS) die('No direct access allowed.');
require_once "config.php";
require_once "private/robokassa_test.php";
require_once "session.php";

$checkInvoice = function() use($mrh_pass1){
    $out_sum = $_REQUEST["OutSum"];
    $inv_id = $_REQUEST["InvId"];
    # $shp_item = $_REQUEST["Shp_item"];
    $crc = $_REQUEST["SignatureValue"];
    $crc = strtoupper($crc);

    # $my_crc = strtoupper(md5("$out_sum:$inv_id:$mrh_pass1:Shp_item=$shp_item"));
    $my_crc = strtoupper(md5("$out_sum:$inv_id:$mrh_pass1"));

    return $crc == $my_crc ? $inv_id : -1;
};

$checkResult = function() use($mrh_pass2){
    $out_sum = $_REQUEST["OutSum"];
    $inv_id = $_REQUEST["InvId"];
    # $shp_item = $_REQUEST["Shp_item"];
    $crc = $_REQUEST["SignatureValue"];
    $crc = strtoupper($crc);

    # $my_crc = strtoupper(md5("$out_sum:$inv_id:$mrh_pass2:Shp_item=$shp_item"));
    $my_crc = strtoupper(md5("$out_sum:$inv_id:$mrh_pass2"));

    return $crc == $my_crc ? $inv_id : -1;
};

$isCashPayment = function(){
    return array_key_exists("cash", $_REQUEST);
};

$_index = function(){
    header("Location: /index.php#hoverblock");
    exit();
};
$_index2 = function(){
    header("Location: /index-free.php#hoverblock");
    exit();
};

$_success = function($data){
    header("Location: /success.php?" . implode("&", array_map(function ($k, $v) {
            return "$k=$v";
        }, array_keys($data), array_values($data))));
    exit();
};

function set_value($key, $value, $valid = true){
    Session::set_value($key, $value, $valid);
}

function get_value($key, $default = ''){
    return Session::get_value($key, $default);
}

function has_error($key){
    return Session::has_error($key);
}

function has_errors(){
    return Session::has_errors();
}

function errors(){
    return Session::errors();
}

function example($templates){
    return $templates[rand(0, count($templates) - 1)];
}