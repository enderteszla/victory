<?php defined('INDIRECT_ACCESS') or define('INDIRECT_ACCESS', true);
require_once "helper.php";
require_once "query.php";
require_once "mailer.php";

session_destroy();
session_start();

set_value('init', false);

$attachment = $_FILES['attachment'];
$out_sum = 0;

$finfo = finfo_open(FILEINFO_MIME_TYPE);
set_value("attachment", $attachment);

$first_name = $_POST['first_name']; // required
set_value("first_name", $first_name, !!$first_name);

$last_name = $_POST['last_name'];
set_value("last_name", $last_name);

$phone = $_POST['phone']; // required && phone
set_value("phone", $phone, !!$phone);

$email = $_POST['email']; // required && email
set_value("email", $email, !!$email && preg_match("/[0-9a-z_]+@[0-9a-z_^\.]+\.[a-z]{2,3}/i", $email));

$info = $_POST['info'];
set_value("info", $info);

$address = $_POST['address'];
set_value("address", $address, !!$address || $delivery == 'pickup');

if(has_errors())
    $_index();

$data = [
    'first_name' => $first_name,
    'last_name' => $last_name,
    'email' => $email,
    'phone' => $phone,
    'info' => $info,
    'delivery' => $delivery,
    'address' => $address,
    'sum' => $out_sum,
    'payment' => $payment
];

$data['inv_id'] = $inv_id = $createOrder($data);
$data['items'] = $items;
$data['attachment'] = $attachment;

sendMail2($data);

session_destroy();
session_start();

set_value('message', "Заказ $inv_id успешно создан.");
$_index();
